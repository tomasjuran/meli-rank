# meli-rank

Simple Django webapp that queries MercadoLibre's API for the top 20 most expensive Smartwatches and top 5 users selling products of that category.

### Install

Simply copy `.env.example` into `.env`, filling in the Django `SECRET_KEY` and MeLi App credentials. Then, run:

```docker-compose up```

Optionally, setup a Sentry project and add `SENTRY_DSN` to `.env`

### Technologies used

- Webapp: **Django**
- Storage/Cache: **Redis**
- Task automation: **Celery**
- Virtualization: **Docker**
- CI / CD: **Github Workflows**
- Monitoring / Error tracking: **Sentry**
- Testing: **Pytest + tox**
- Routing / Reverse Proxy: **Traefik**


### Future work

- **In-progress** add Gitlab CI/CD for automated testing
- Bundle meli-sdk into its own library (setup a private repository e.g. **CodeArtifact**)
- Setup **Cloudwatch** for persistent and high-availability logging
- Let the user do more than just signing in :)
