import os

from celery import Celery

from core.settings import CELERY_SCHEDULE_INTERVAL

# Set the default Django settings module for the "celery" program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")

app = Celery("core")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace="CELERY" means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django apps.
app.autodiscover_tasks()

periodic_tasks = ["get_seller_rank", "get_item_rank"]

# Schedule tasks to run every INTERVAL seconds
app.conf.beat_schedule = {task: {"task": task, "schedule": CELERY_SCHEDULE_INTERVAL} for task in periodic_tasks}
