from os import getenv

MELI_API_ROOT = getenv("MELI_API_ROOT", "https://api.mercadolibre.com")
MELI_AUTH_ROOT = getenv("MELI_AUTH_ROOT", "https://auth.mercadolibre.com")
REQUEST_BACKOFF = float(getenv("REQUEST_BACKOFF", "0.001"))  # Time to wait (in seconds) between requests
REQUEST_TIMEOUT = 5  # Seconds to wait for a request to finish

DEFAULT_SITE = getenv("DEFAULT_SITE", "MLA")  # Mercado Libre Argentina
DEFAULT_ITEMS_PER_REQUEST_LIMIT = int(getenv("DEFAULT_ITEMS_PER_REQUEST_LIMIT", "50"))  # Max items per request
