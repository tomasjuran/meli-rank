from urllib.parse import urlencode

from .constants import DEFAULT_SITE
from .models import Item, ItemAPIRequestParams, MeliUser
from .utils import generate_api_url, generate_auth_url, get_paginated_content, post, request

SITES_URL = generate_api_url("sites", "{}", "search")  # e.g. https://api.mercadolibre.com/sites/MLA/search
TOKEN_URL = generate_api_url("oauth", "token")  # https://api.mercadolibre.com/oauth/token
USERS_URL = generate_api_url("users", "me")  # https://api.mercadolibre.com/users/me
AUTH_URL = generate_auth_url("authorization")  # https://auth.mercadolibre.com/authorization


def get_items_by_site(
    site: str = DEFAULT_SITE, params: ItemAPIRequestParams = ItemAPIRequestParams(), max_items: int = None
) -> list[Item]:
    url = SITES_URL.format(site)
    return [Item.from_dict(item) for item in get_paginated_content(url=url, params=params, max_items=max_items)]


def request_authorization_code_URL(app_id: str, redirect_uri: str) -> str:
    params = {  # query string
        "response_type": "code",
        "client_id": app_id,
        "redirect_uri": redirect_uri,
    }
    return f"{AUTH_URL}?{urlencode(params)}"


def get_access_token(app_id: str, redirect_uri: str, client_secret: str, authorization_code: str) -> dict:
    headers = {
        "accept": "application/json",
        "content-type": "application/x-www-form-urlencoded",
    }
    data = {
        "grant_type": "authorization_code",
        "client_id": app_id,
        "client_secret": client_secret,
        "code": authorization_code,
        "redirect_uri": redirect_uri,
    }
    return post(url=TOKEN_URL, headers=headers, data=data).json()


def get_user_information(access_token: str) -> MeliUser:
    return MeliUser.from_dict(request(url=USERS_URL, headers={"Authorization": f"Bearer {access_token}"}).json())
