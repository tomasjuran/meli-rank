from dataclasses import asdict, dataclass
from typing import Literal, Optional, Self

from .constants import DEFAULT_ITEMS_PER_REQUEST_LIMIT


@dataclass
class Seller:
    id: str
    nickname: str
    permalink: str

    @classmethod
    def from_dict(cls, data: dict) -> Self:
        return cls(
            id=data["id"],
            nickname=data["nickname"],
            permalink=data["permalink"],
        )

    def as_dict(self) -> dict:
        return as_dict(self)


@dataclass
class Item:
    id: str
    title: str
    permalink: str
    thumbnail: str
    currency_id: str
    price: float
    sold_quantity: int
    seller: Seller

    def __post_init__(self):
        if not isinstance(self.seller, Seller):
            self.seller = Seller.from_dict(self.seller)

    @classmethod
    def from_dict(cls, data: dict) -> Self:
        return cls(
            id=data["id"],
            title=data["title"],
            permalink=data["permalink"],
            thumbnail=data["thumbnail"],
            currency_id=data["currency_id"],
            price=data["price"],
            sold_quantity=data["sold_quantity"],
            seller=data["seller"],
        )

    def as_dict(self) -> dict:
        d = as_dict(self)
        d["seller"] = self.seller.as_dict()
        return d


@dataclass
class MeliUser:
    id: str
    nickname: str
    first_name: str
    last_name: str
    email: str

    @classmethod
    def from_dict(cls, data: dict) -> Self:
        return cls(
            id=data["id"],
            nickname=data["nickname"],
            first_name=data["first_name"],
            last_name=data["last_name"],
            email=data["email"],
        )

    def as_dict(self) -> dict:
        return as_dict(self)


@dataclass
class APIRequestParams:
    offset: int = 0
    limit: int = DEFAULT_ITEMS_PER_REQUEST_LIMIT


@dataclass
class ItemAPIRequestParams(APIRequestParams):
    category: Optional[str] = None
    sort: Optional[Literal["price_asc", "price_desc"]] = None


@dataclass
class PaginationInfo:
    total: int
    primary_results: int
    offset: int
    limit: int

    def as_dict(self) -> dict:
        return as_dict(self)


@dataclass
class APIResponse:
    pagination: PaginationInfo
    results: list

    def __post_init__(self):
        if not isinstance(self.pagination, PaginationInfo):
            self.pagination = PaginationInfo(**self.pagination)

    @classmethod
    def from_dict(cls, data: dict) -> Self:
        return cls(results=data["results"], pagination=data["paging"])

    def as_dict(self) -> dict:
        d = as_dict(self)
        d["pagination"] = self.pagination.as_dict()
        return d


def as_dict(instance, unfiltered=True) -> dict:
    """Returns a dict with instance attributes

    Args:
        instance: a dataclass instance
        unfiltered (bool, optional): if set to False, filter attributes whose value is None. Defaults to True.
    """
    return asdict(instance, dict_factory=lambda x: {k: v for (k, v) in x if unfiltered or v is not None})
