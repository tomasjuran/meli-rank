import pytest

from meli_sdk.models import Item, Seller


@pytest.fixture
def mock_api_response():
    return [
        Item(
            id=entry[0],
            title=entry[1],
            permalink="https://localhost:8000",
            thumbnail="https://localhost:8000",
            currency_id="ARS",
            price=entry[2],
            sold_quantity=entry[3],
            seller=Seller(id=entry[4], nickname=entry[5], permalink="https://localhost:8000"),
        )
        for entry in [
            ("1", "Apple Watch", 2999700, 0, "1", "MERC"),
            ("2", "Suunto Smartwatch Red", 2231149, 0, "2", "ELEC"),
            ("3", "Suunto Smartwatch Blue", 2231149, 1, "2", "ELEC"),
            ("4", "Suunto Smartwatch Magenta", 2231149, 10, "2", "ELEC"),
            ("5", "Smartwatch Tag Heuer", 2199999, 20, "3", "IT"),
            ("6", "Smartwatch Marq Athlete", 2020999, 15, "1", "MERC"),
            ("7", "Apple Watch Series 7", 1661277.85, 0, "1", "MERC"),
            ("8", "Huawei Reloj 3 Pro", 1504416.69, 5, "4", "TEC"),
            ("9", "Tactix Delta Solar", 1419999, 10, "5", "SALES"),
            ("10", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("11", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("12", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("13", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("14", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("15", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("16", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("17", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("18", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("19", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("20", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("21", "Apple Watch Ultra", 1258859, 1, "6", "FIT"),
        ]
    ]
