import logging
import math
import time

import requests

from .constants import MELI_API_ROOT, MELI_AUTH_ROOT, REQUEST_BACKOFF, REQUEST_TIMEOUT
from .models import APIRequestParams, APIResponse, PaginationInfo, as_dict


def request(url: str, params: dict = {}, headers: dict = {}, timeout=REQUEST_TIMEOUT) -> requests.Response:
    response = requests.get(url=url, params=params, headers=headers, timeout=timeout)
    if response.status_code != 200:
        logging.error("Request failed on URL '%s' with params=%s, headers=%s,", url, params, headers)
        response.raise_for_status()
    return response


def post(url: str, headers: dict = {}, data: dict = {}, timeout=REQUEST_TIMEOUT) -> requests.Response:
    response = requests.post(url=url, headers=headers, data=data, timeout=timeout)
    if response.status_code != 200:
        logging.error("Request failed on URL '%s' with headers=%s, data=%s", url, headers, data)
        response.raise_for_status()
    return response


def api_request(url: str, params: APIRequestParams = None) -> dict:
    request_params = {} if params is None else as_dict(params, unfiltered=False)
    return request(url=url, params=request_params).json()


def get_paginated_content(url: str, params: APIRequestParams, max_items: int = None) -> list:
    """Loops over pages, concatenating and returning their results"""
    current_page_request: APIRequestParams = params
    max_results = max_items if max_items is not None else math.inf
    results = []
    while current_page_request is not None and len(results) < max_results:
        params.offset = current_page_request.offset
        params.limit = current_page_request.limit
        try:
            api_response = APIResponse.from_dict(api_request(url=url, params=params))
            if len(api_response.results) <= 0:
                break
            results.extend(api_response.results)
            current_page_request = next_page(api_response.pagination)
            if current_page_request is not None:
                time.sleep(REQUEST_BACKOFF)
        except Exception:
            logging.exception()
            break
    if max_items is not None:
        results = results[:max_items]
    return results


def next_page(current_page: PaginationInfo, is_public_access=True) -> APIRequestParams | None:
    """
    Returns offset and limit for next page, or None if there are no more pages
    Assumes we are limited by public access (50 per page and 1000 in total)
    """
    max_items = current_page.primary_results if is_public_access else current_page.total
    items_processed = current_page.offset + current_page.limit
    items_left = max_items - items_processed
    if items_left <= 0:
        return
    return APIRequestParams(offset=items_processed, limit=min(items_left, current_page.limit))


def join_path(fragments: list[str]) -> str:
    """Returns a single url given path fragments"""
    return "/".join(s.strip("/") for s in fragments)


def generate_api_url(*args) -> str:
    """Generates a valid API URL for a given path"""
    return join_path([MELI_API_ROOT, *args])


def generate_auth_url(*args) -> str:
    """Generates a valid Auth URL for a given path"""
    return join_path([MELI_AUTH_ROOT, *args])
