from django.apps import AppConfig


class MelirankConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "melirank"
