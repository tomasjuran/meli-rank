import json

from celery import shared_task
from django.core.cache import cache
from meli_sdk.meli_sdk import get_items_by_site
from meli_sdk.models import ItemAPIRequestParams

from core.settings import CACHE_EXPIRE


@shared_task(name="get_seller_rank")
def get_seller_rank():
    item_list = get_items_by_site(params=ItemAPIRequestParams(category="MLA352679"))
    sellers = {}
    for item in item_list:
        seller = item.seller
        if seller.id not in sellers:
            sellers[seller.id] = {
                "seller": seller.as_dict(),
                "sold": 0,
                "unique_items": set(),
                "most_sales": item,
            }
        # Filter repeats
        if item.id not in sellers[seller.id]["unique_items"]:
            sellers[seller.id]["unique_items"].add(item.id)
            sellers[seller.id]["sold"] += item.sold_quantity
            # Record item with most sales for that seller
            if item.sold_quantity > sellers[seller.id]["most_sales"].sold_quantity:
                sellers[seller.id]["most_sales"] = item

    best_sellers: list[dict] = sorted(sellers.values(), key=lambda entry: entry["sold"], reverse=True)[:5]
    # Convert items back to dict for serialization
    for entry in best_sellers:
        entry["most_sales"] = entry["most_sales"].as_dict()
        entry.pop("unique_items")
    cache.set("seller_rank", json.dumps(best_sellers), CACHE_EXPIRE)


@shared_task(name="get_item_rank")
def get_item_rank():
    item_list = get_items_by_site(params=ItemAPIRequestParams(category="MLA352679", sort="price_desc"), max_items=20)
    cache.set("item_rank", json.dumps([item.as_dict() for item in item_list]), CACHE_EXPIRE)
