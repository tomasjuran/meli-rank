import pytest

import json

from meli_sdk.models import Item, Seller
from melirank import views, tasks


@pytest.fixture(autouse=True)
def use_dummy_cache_backend(settings):
    settings.CACHES = {
        "default": {
            "BACKEND": "django.core.cache.backends.dummy.DummyCache",
        }
    }


@pytest.fixture
def mock_api_response():
    return [
        Item(
            id=entry[0],
            title=entry[1],
            permalink="https://localhost:8000",
            thumbnail="https://localhost:8000",
            currency_id="ARS",
            price=entry[2],
            sold_quantity=entry[3],
            seller=Seller(id=entry[4], nickname=entry[5], permalink="https://localhost:8000"),
        )
        for entry in [
            ("1", "Apple Watch", 2999700, 0, "1", "MERC"),
            ("2", "Suunto Smartwatch Red", 2231149, 0, "2", "ELEC"),
            ("3", "Suunto Smartwatch Blue", 2231149, 1, "2", "ELEC"),
            ("4", "Suunto Smartwatch Magenta", 2231149, 10, "2", "ELEC"),
            ("5", "Smartwatch Tag Heuer", 2199999, 20, "3", "IT"),
            ("6", "Smartwatch Marq Athlete", 2020999, 15, "1", "MERC"),
            ("7", "Apple Watch Series 7", 1661277.85, 0, "1", "MERC"),
            ("8", "Huawei Reloj 3 Pro", 1504416.69, 5, "4", "TEC"),
            ("9", "Tactix Delta Solar", 1419999, 10, "5", "SALES"),
            ("10", "Apple Watch Se", 1260000, 0, "6", "FIT"),
            ("11", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("12", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("13", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("14", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("15", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("16", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("17", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("18", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("19", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("20", "Apple Watch Se", 1260000, 1, "6", "FIT"),
            ("21", "Apple Watch Ultra", 1258859, 2, "6", "FIT"),
        ]
    ]


@pytest.fixture
def mock_item_rank_model(mock_api_response):
    return mock_api_response[:20]


@pytest.fixture
def mock_item_rank_json(mock_item_rank_model):
    return json.dumps([item.as_dict() for item in mock_item_rank_model])


@pytest.fixture
def mock_best_sellers_dict():
    return [
        {
            "seller": {"id": entry[0], "nickname": entry[1], "permalink": "https://localhost:8000"},
            "sold": entry[2],
            "most_sales": {
                "id": entry[3],
                "title": entry[4],
                "permalink": "https://localhost:8000",
                "thumbnail": "https://localhost:8000",
                "currency_id": "ARS",
                "price": entry[5],
                "sold_quantity": entry[6],
                "seller": {"id": entry[0], "nickname": entry[1], "permalink": "https://localhost:8000"},
            },
        }
        for entry in [
            ("3", "IT", 20, "5", "Smartwatch Tag Heuer", 2199999, 20),
            ("1", "MERC", 15, "6", "Smartwatch Marq Athlete", 2020999, 15),
            ("6", "FIT", 12, "21", "Apple Watch Ultra", 1258859, 2),
            ("2", "ELEC", 11, "4", "Suunto Smartwatch Magenta", 2231149, 10),
            ("5", "SALES", 10, "9", "Tactix Delta Solar", 1419999, 10),
        ]
    ]


@pytest.fixture
def mock_best_sellers_json(mock_best_sellers_dict):
    return json.dumps(mock_best_sellers_dict)


@pytest.fixture
def mock_best_sellers_model(mock_best_sellers_dict):
    for entry in mock_best_sellers_dict:
        entry["seller"] = Seller.from_dict(entry["seller"])
        entry["most_sales"] = Item.from_dict(entry["most_sales"])
    return mock_best_sellers_dict


@pytest.fixture
def mock_cache(monkeypatch):
    def factory(module):
        class MockCache:
            def __init__(self, *args, **kwargs):
                self.d = {}

            def set(self, key, value, *args, **kwargs):
                self.d[key] = value

            def get(self, key, *args, **kwargs):
                return self.d[key]

        mock_result = MockCache()
        monkeypatch.setattr(module, "cache", mock_result)
        return mock_result

    return factory


@pytest.fixture
def mock_views_cache(mock_cache):
    return mock_cache(views)


@pytest.fixture
def mock_tasks_cache(mock_cache):
    return mock_cache(tasks)
