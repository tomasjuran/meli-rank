from django.http import HttpResponse
from django.test import Client, SimpleTestCase
from django.urls import reverse
from unittest.mock import MagicMock

from meli_sdk.models import MeliUser
from melirank import views, tasks


def test_seller_rank_view_loads_data_correctly(
    client: Client, mock_views_cache, mock_best_sellers_json, mock_best_sellers_model
):
    mock_views_cache.set("seller_rank", mock_best_sellers_json)
    response: HttpResponse = client.get(reverse("seller_rank"))
    assert response.status_code == 200
    assert response.context["seller_list"] == mock_best_sellers_model


def test_item_rank_view_loads_data_correctly(
    client: Client, mock_views_cache, mock_item_rank_json, mock_item_rank_model
):
    mock_views_cache.set("item_rank", mock_item_rank_json)
    response: HttpResponse = client.get(reverse("item_rank"))
    assert response.status_code == 200
    assert response.context["item_list"] == mock_item_rank_model


def test_signin_redirects_correctly(client: Client):
    response: HttpResponse = client.get(reverse("signin"))
    SimpleTestCase().assertRedirects(
        response,
        "http://MELI_AUTH_ROOT/authorization?response_type=code&client_id=MELI_APP_ID&redirect_uri=http://MELI_REDIRECT_URI",
        fetch_redirect_response=False,
    )


def test_meli_oauth2_signs_user_in(monkeypatch, client: Client):
    access_token = "APP_USR-5387223166827464-090515-8cc4448aac10d5105474e135355a8321-8035443"
    mock_get_access_token = MagicMock(
        return_value={
            "access_token": access_token,
            "token_type": "bearer",
            "expires_in": 10800,
            "scope": "offline_access read write",
            "user_id": 8035443,
            "refresh_token": "TG-5b9032b4e4b0714aed1f959f-8035443",
        }
    )
    monkeypatch.setattr(
        views,
        views.get_access_token.__name__,
        mock_get_access_token,
    )
    mock_get_user_information_call = MagicMock(
        return_value=MeliUser(id="1", nickname="test", first_name="Test", last_name="tesT", email="admin@mail.com")
    )
    monkeypatch.setattr(views, views.get_user_information.__name__, mock_get_user_information_call)

    client.get(reverse("oauth2") + "?code=code")
    mock_get_access_token.assert_called_once()
    mock_get_user_information_call.assert_called_once_with(access_token)


def test_get_seller_rank_task(monkeypatch, mock_api_response, mock_tasks_cache, mock_best_sellers_json):
    mock_get_items_api_call = MagicMock(return_value=mock_api_response)
    monkeypatch.setattr(tasks, tasks.get_items_by_site.__name__, mock_get_items_api_call)
    tasks.get_seller_rank()
    assert mock_tasks_cache.get("seller_rank") == mock_best_sellers_json


def test_get_item_rank_task(monkeypatch, mock_item_rank_model, mock_tasks_cache, mock_item_rank_json):
    mock_get_items_api_call = MagicMock(return_value=mock_item_rank_model)
    monkeypatch.setattr(tasks, tasks.get_items_by_site.__name__, mock_get_items_api_call)
    tasks.get_item_rank()
    assert mock_tasks_cache.get("item_rank") == mock_item_rank_json
