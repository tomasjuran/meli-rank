from django.urls import path

from . import views

urlpatterns = [
    path("", views.seller_rank, name="seller_rank"),
    path("item-rank", views.item_rank, name="item_rank"),
    path("signin", views.signin, name="signin"),
    path("signout", views.signout, name="signout"),
    path("oauth2", views.oauth2, name="oauth2"),
]
