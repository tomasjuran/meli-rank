import json
import logging

from django.shortcuts import render, redirect
from django.core.cache import cache
from django.http import HttpRequest
from meli_sdk.meli_sdk import get_access_token, get_user_information, request_authorization_code_URL
from meli_sdk.models import Item, Seller

from core.settings import MELI_APP_ID, MELI_CLIENT_SECRET, MELI_REDIRECT_URI


def seller_rank(request: HttpRequest):
    best_sellers = json.loads(cache.get("seller_rank", default="[]"))
    for entry in best_sellers:
        entry["seller"] = Seller.from_dict(entry["seller"])
        entry["most_sales"] = Item.from_dict(entry["most_sales"])
    context = {
        "seller_list": best_sellers,
    }
    return render(request, "melirank/sellers.html", context)


def item_rank(request: HttpRequest):
    item_list = json.loads(cache.get("item_rank", default="[]"))
    context = {
        "item_list": [Item.from_dict(item) for item in item_list],
    }
    return render(request, "melirank/index.html", context)


def signin(request: HttpRequest):
    username = request.session.get("username", default=None)
    if username is not None:
        return redirect("/")

    auth_url = request_authorization_code_URL(app_id=MELI_APP_ID, redirect_uri=MELI_REDIRECT_URI)
    logging.info("Redirecting to %s", auth_url)
    return redirect(auth_url)


def signout(request: HttpRequest):
    request.session.pop("username", default=None)
    return redirect("/")


def oauth2(request: HttpRequest):
    authorization_code = request.GET.get("code", default=None)
    if authorization_code is None:
        logging.error("Could not authorize user when redirected to path=%s", request.get_full_path())
        return redirect("/")

    token_dict = get_access_token(
        app_id=MELI_APP_ID,
        redirect_uri=MELI_REDIRECT_URI,
        client_secret=MELI_CLIENT_SECRET,
        authorization_code=authorization_code,
    )
    if token_dict is None or "access_token" not in token_dict:
        logging.error("Could not authorize user when requesting access token. Received %s", token_dict)
        return redirect("/")

    user_information = get_user_information(token_dict["access_token"])
    if user_information is None:
        logging.error("Could not retrieve user information using access token %s", token_dict["access_token"])
        return redirect("/")

    request.session["username"] = user_information.first_name
    return redirect("/")
